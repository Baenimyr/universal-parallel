cmake_minimum_required(VERSION 3.20)
project(parallel LANGUAGES CXX)

set(CMAKE_CXX_VISIBILITY_PRESET "hidden")
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_VISIBILITY_INLINES_HIDDEN ON)

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

# Options
option(BUILD_TEST "tests" OFF)

# Code
add_subdirectory(src/main)

# Test
if(BUILD_TEST)
enable_testing()
add_subdirectory(src/test)
endif(BUILD_TEST)

# ----- Installation -----
install(TARGETS ${PROJECT_NAME}
EXPORT parallel
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
)
install(EXPORT parallel FILE UniversalParallelTargets.cmake
    DESTINATION "share/cmake"
    NAMESPACE "Universal::")
configure_package_config_file(${CMAKE_SOURCE_DIR}/cmake/ParallelConfig.in.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/ParallelConfig.cmake
    INSTALL_DESTINATION "share/cmake"
)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/ParallelConfig.cmake
    DESTINATION "share/cmake"
)
