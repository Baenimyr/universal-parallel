# Parallel
Ce projet implémente des conteneur compatibles avec des opérations concurrentes, une thread_pool et un système de tâches dépendantes entre-elles inspiré des [_CompletableFuture_](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/concurrent/CompletableFuture.html) de java.

## CompletableFuture
Cette structure est chargé d’exécuter une opération dans un arbre de dépendances.
Une chaîne de dépendance commence avec `CompletableFuture::completed` ou `CompletableFuture::supply`.
D’autres opérations qui utiliseront la valeur de l’élément précédant peuvent être chaînées.
Le chaînage est linéaire sauf pour `thenAcceptBoth` et `thenCombine` qui utilisent deux valeurs en entrées pour le calcul.

Le `CompletableFuture` n’est pas paresseux et exécutera son contenu dès que possible selon ses dépendances et la disponibilité des unités de calcul.
Le code sera exécuté par un _Executor_ ou dans le thread courant si l’exécuteur est nul.

Un `CompletableFuture<void>` ne contiendra jamais de valeur mais permet de synchroniser les évènements, comme attendre que des opérations précédentes soient terminées.

:warning: les `CompletableFuture` ne sont qu’une interface vers un objet partagé contenant la fonction et le résultat.
La destruction de `CompletableFuture` d’une chaîne ne détruit pas la chaîne et n’arrête pas son exécution tant que le dernier élément existe.
Cependant si toutes les références sont perdues alors les nœuds seront détruits et l’exécution s’arrêtera.