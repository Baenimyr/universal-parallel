#include "universal/parallel/completable_future.hpp"

#include <cassert>
#include <memory>
#include <variant>

namespace universal::parallel {

CompletableFuture_data::CompletableFuture_data(decltype(CompletableFuture_data::val) && val)
	: action(ActionExecution()), val(std::move(val)) {}

CompletableFuture_data::CompletableFuture_data(ActionAttente action)
	: action(std::move(action)), val() {}

bool CompletableFuture_data::isPending() const {
	return std::holds_alternative<ActionAttente>(action);
}

void CompletableFuture_data::set(decltype(val) && valeur) {
	this->val = std::move(valeur);
	/** Relâche la référence vers le support d’exécution pour vérifier que isDone(). */
	this->action = ActionExecution();
}

void CompletableFuture_data::notify() {
	// si l’action n’a pas été déclenchée et est prête
	if(isPending() && std::get<ActionAttente>(this->action)->pret()) {
		std::shared_ptr<IAction> action;
		{
			std::lock_guard lg(_mutex);
			if(!isPending())
				return;
			action = std::get<ActionAttente>(std::move(this->action));
			this->action = ActionExecution(action);
		}
		assert(action.unique());

		if((bool)executor) {
			executor->execute([a = std::move(action)]() { a->run(); });
		} else { // execute immédiatement
			assert(std::holds_alternative<ActionExecution>(this->action));
			assert(!std::get<ActionExecution>(this->action).expired());
			assert(!action->_cible.expired());
			action->run();
		}
	}
} //// CompletableFuture_::CompletableFuture_data::notify()

void CompletableFuture_data::_notify_all() {
	this->_cv.notify_all();
	if(!pendings.empty()) {
		std::lock_guard lg(_mutex);
		for(auto & cf : pendings)
			if(auto s_cf = cf.lock())
				s_cf->notify();
		pendings.clear();
	}
} //// CompletableFuture_data::_notify_all()

//
//
//
CompletableFuture_::CompletableFuture_(data_t && valeur)
	: _shared_data(std::make_shared<CompletableFuture_data>(std::move(valeur))) {}

CompletableFuture_::CompletableFuture_(ActionAttente && action)
	: _shared_data(std::make_shared<CompletableFuture_data>(std::move(action))) {
	std::get<ActionAttente>(this->_shared_data->action)->_cible = this->_shared_data;
}

CompletableFuture_::CompletableFuture_(std::nested_exception && erreur)
	: _shared_data(std::make_shared<CompletableFuture_data>(std::move(erreur))) {}

CompletableFuture_::CompletableFuture_(CompletableFuture_ const & cf)
	: _shared_data(cf._shared_data) {}

CompletableFuture_::CompletableFuture_(CompletableFuture_ && cf)
	: _shared_data(std::move(cf._shared_data)) {}

CompletableFuture_ & CompletableFuture_::operator=(CompletableFuture_ const & cf) {
	this->_shared_data = cf._shared_data;
	return *this;
}

CompletableFuture_ & CompletableFuture_::operator=(CompletableFuture_ && cf) {
	this->_shared_data = std::move(cf._shared_data);
	return *this;
}

bool CompletableFuture_::isPending() const {
	struct {
		bool operator()(ActionAttente const & a) { return (bool)a; }

		bool operator()(ActionExecution const & a) { return false; }
	} static pending;

	return _shared_data && std::visit(pending, this->_shared_data->action);
} //// CompletableFuture_::isPending()

bool CompletableFuture_::isRunning() const {
	struct {
		bool operator()(ActionAttente const & a) { return false; }

		bool operator()(ActionExecution const & a) { return !a.expired(); }
	} static pending;

	return _shared_data && std::visit(pending, this->_shared_data->action);
} //// CompletableFuture_::isRunning()

bool CompletableFuture_::isDone() const {
	struct {
		bool operator()(ActionAttente const & a) { return false; }

		bool operator()(ActionExecution const & a) { return a.expired(); }
	} static done;

	return _shared_data && std::visit(done, this->_shared_data->action);
} //// CompletableFuture_::isDone()

bool CompletableFuture_::isCancelled() const {
	struct {
		bool operator()(ActionAttente const & a) { return !(bool)a; }

		bool operator()(ActionExecution const & a) { return false; }
	} static cancel;

	return _shared_data && std::visit(cancel, this->_shared_data->action);
} //// CompletableFuture_::isCancelled()

void CompletableFuture_::wait() const noexcept {
	if(!isDone()) {
		std::unique_lock lg(_shared_data->_mutex);
		if(!isDone()) {
			_shared_data->_cv.wait(lg, [this]() { return this->isDone(); });
		}
	}
} //// CompletableFuture_::wait()

bool CompletableFuture_::wait(std::chrono::milliseconds timeout) const noexcept {
	if(isDone())
		return true;
	{
		std::unique_lock lg(_shared_data->_mutex);
		if(!isDone()) {
			_shared_data->_cv.wait_for(lg, timeout);
		}
	}
	return isDone();
} //// CompletableFuture_::wait(std::chrono::milliseconds)

bool CompletableFuture_::complete_exception(std::nested_exception const & ex) {
	if(!_shared_data->isPending())
		return false;
	{
		std::lock_guard lg(_shared_data->_mutex);
		if(!_shared_data->isPending())
			return false;
		_shared_data->set(ex);
	}
	_shared_data->_notify_all();
	return true;
} //// CompletableFuture_::complete_exception

bool CompletableFuture_::cancel() {
	if(!_shared_data->isPending())
		return false;
	{
		std::lock_guard lg(_shared_data->_mutex);
		if(!_shared_data->isPending())
			return false;
		_shared_data->action = ActionAttente();
	}
	_shared_data->_notify_all();
	return true;
} //// CompletableFuture_::cancel()

void CompletableFuture_::add_dependant(CompletableFuture_ const & dep) const {
	if(isDone())
		dep._shared_data->notify();
	else {
		std::lock_guard lg(_shared_data->_mutex);
		_shared_data->pendings.emplace_back(dep._shared_data);
	}
}

//
//
//
CompletableFuture<void> all(std::vector<CompletableFuture_> cond) {
	struct all_action final: public IAction {
		std::vector<CompletableFuture_> _conditions;

		all_action(std::vector<CompletableFuture_> const & cond): _conditions(cond) {}

		bool pret() const override {
			return std::all_of(_conditions.begin(),
			  _conditions.end(),
			  [](CompletableFuture_ const & cf) { return cf.isDone(); });
		}

		void run() override {
			if(auto cible = _cible.lock()) {
				cible->set(data_t{});
				cible->_notify_all();
			}
		}
	};

	auto action = std::make_shared<all_action>(cond);
	CompletableFuture<void> c(ActionAttente(std::move(action)));
	for(auto const & pre : cond) {
		pre.add_dependant(c);
	}
	return c;
}

CompletableFuture<void> any(std::vector<CompletableFuture_> cond) {
	struct any_action final: public IAction {
		std::vector<CompletableFuture_> _conditions;

		any_action(std::vector<CompletableFuture_> cond): _conditions(std::move(cond)) {}

		bool pret() const override {
			return std::any_of(_conditions.begin(),
			  _conditions.end(),
			  [](CompletableFuture_ const & cf) { return cf.isDone(); });
		}

		void run() override {
			if(auto cible = _cible.lock()) {
				cible->set(data_t{});
				cible->_notify_all();
			}
		}
	};

	CompletableFuture<void> c(ActionAttente(std::make_shared<any_action>(cond)));
	for(auto const & pre : cond) {
		pre.add_dependant(c);
	}
	return c;
}

CompletableFuture<void>::CompletableFuture(): CompletableFuture_(data_t()) {}

bool CompletableFuture<void>::complete() {
	if(!_shared_data->isPending())
		return false;
	{
		std::unique_lock lg(_shared_data->_mutex);
		if(!_shared_data->isPending())
			return false;
		_shared_data->set(data_t(nullptr));
	}
	_shared_data->_notify_all();
	return true;
} //// CompletableFuture<void>::complete()

bool CompletableFuture<void>::complete(std::chrono::milliseconds timeout) {
	if(!_shared_data->isPending())
		return false;
	{
		std::unique_lock lg(_shared_data->_mutex);
		if(!_shared_data->isPending())
			return false;
		_shared_data->_cv.wait_for(lg, timeout);
		if(!_shared_data->isPending())
			return false;
		_shared_data->set(data_t(nullptr));
	}
	_shared_data->_notify_all();
	return true;
} //// CompletableFuture<void>::complete(std::chrono::milliseconds)

CompletableFuture<void> CompletableFuture<void>::completed() {
	return CompletableFuture<void>();
} //// CompletableFuture<void>::completed()

} // namespace universal::parallel
