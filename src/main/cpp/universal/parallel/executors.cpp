#include "universal/parallel/executors.hpp"

#include <exception> // std::exception
#include <iostream>

namespace universal::parallel {
// ----- ExecuteurImmediat -----
void ExecuteurImmediat::execute(std::move_only_function<void(void)> && f) noexcept {
	try {
		f();
	} catch(std::exception & e) {
		std::cerr << "ExecuteurImmediat error: " << e.what() << std::endl;
	} catch(...) {
		std::cerr << "ExecuteurImmediat error" << std::endl;
	}
} //// ExecuteurImmediat::execute(std::move_only_function<void(void)> &&)

// ----- ExecuteurOrphelin -----
void ExecuteurOrphelin::execute(std::move_only_function<void(void)> && f) noexcept {
	std::lock_guard lg(this->_mutex);
	this->operations.push_back(std::move(f));
} //// ExecuteurOrphelin::execute(std::move_only_function<void(void)> &&)

void ExecuteurOrphelin::run() noexcept {
	std::move_only_function<void(void)> f;

	{
		std::lock_guard lg(this->_mutex);
		if(!this->empty()) {
			f = std::move(this->operations.front());
			this->operations.pop_front();
		}
	}
	if(f) {
		try {
			f();
		} catch(std::exception & e) {
			std::cerr << "ExecuteurOrphelin error: " << e.what() << std::endl;
		} catch(...) {
			std::cerr << "ExecuteurOrphelin error" << std::endl;
		}
	}
} //// ExecuteurOrphelin::run()

void ExecuteurOrphelin::run(bool (*test)() noexcept) noexcept {
	while(test()) {
		run();
	}
}

} // namespace universal::parallel
