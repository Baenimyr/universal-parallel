#include "universal/parallel/thread_pool.hpp"

#include <cassert>
#include <functional> // std::bind_front
#include <iostream> // std::cerr
#include <stdexcept> // std::invalid_argument
#include <thread>

namespace universal::parallel {

thread_pool::thread_pool_unit::thread_pool_unit(thread_pool * tp) noexcept
	// BUG le thread démarre trop tôt
	: std::jthread(), pool(tp) {
	std::jthread::operator=(std::jthread(std::bind_front(&thread_pool_unit::operator(), this)));
}

void thread_pool::thread_pool_unit::operator()(std::stop_token stop) noexcept {
	try {
		while(true) {
			if(stop.stop_requested())
				goto fin;
			auto a = this->pool->tache();
			if(!a)
				goto fin;

			this->etat = CALCUL;
			try {
				a();
			} catch(...) {
				break;
			}
			this->etat = ATTENTE;
		}
	} catch(...) {
		std::cerr << "Erreur sur le thread " << std::this_thread::get_id() << std::endl;
	}

fin:
	this->etat = activite::TERMINE;
	return; // Termine le thread
} //// thread_pool::thread_pool_unit::operator()()

thread_pool::thread_pool(size_t t) noexcept: max_thread(t), _unites(max_thread) {
	// Ne démarre les threads qu’avec l’arrivé de nouvelles tâches
} //// thread_pool::thread_pool(size_t)

thread_pool::~thread_pool() {
	etat_ = EXTINCTION;
	this->_taches_cv.notify_all();

	{
		std::unique_lock w_lock(this->_unites_mutex);
		for(auto & unit : this->_unites)
			if(unit && unit->joinable())
				unit->join();
	}
	etat_ = TERMINE;
} //// thread_pool::~thread_pool()

bool thread_pool::unite_inactive(std::unique_ptr<thread_pool::thread_pool_unit> const & unit) {
	return !unit || unit->etat == thread_pool_unit::activite::TERMINE;
}

void thread_pool::nouvelle_unite() {
	std::unique_lock w_lock(this->_unites_mutex);
	auto n = std::find_if(this->_unites.begin(), this->_unites.end(), unite_inactive);
	assert(n != this->_unites.end());
	*n = std::make_unique<thread_pool_unit>(this);
	w_lock.unlock();
} //// thread_pool::nouvelle_unite()

size_t thread_pool::size() const noexcept {
	std::shared_lock r_lock(this->_unites_mutex);
	return (
	  size_t)std::count_if(this->_unites.begin(), this->_unites.end(), std::not_fn(unite_inactive));
}

void thread_pool::execute(std::move_only_function<void()> && f) {
	if(etat_ != activite::PRET)
		throw std::invalid_argument("ThreadPool not running.");
	assert(f);
	{
		std::lock_guard w_lock(this->_taches_mutex);
		this->_taches.emplace_back(std::move(f));
	}
	this->_taches_cv.notify_all();

	{
		std::shared_lock r_lock(this->_unites_mutex);
		// Il y a trop d’unités de travail
		if((size_t)std::count_if(this->_unites.begin(),
			 this->_unites.end(),
			 std::not_fn(unite_inactive)) >= max_thread)
			return;
		// une unité est toujours disponible
		else if(std::any_of(this->_unites.begin(), this->_unites.end(), [](auto const & u) -> bool {
					return u && u->etat == thread_pool_unit::ATTENTE;
				}))
			return;
	}
	this->nouvelle_unite();
} //// thread_pool::execute

std::move_only_function<void()> thread_pool::tache() noexcept {
	std::unique_lock w_lock(this->_taches_mutex);
	while(etat_ == thread_pool::activite::PRET && this->_taches.empty())
		this->_taches_cv.wait_for(w_lock, this->survie);

	if(this->_taches.empty())
		return {};
	else {
		auto f = std::move(this->_taches.front());
		this->_taches.pop_front();
		return f;
	}
}

}; // namespace universal::parallel
