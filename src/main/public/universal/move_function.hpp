#pragma once
#include <functional>
#include <memory> // std::unique_ptr
#include <type_traits> // std::is_invokable

#ifndef __cpp_lib_move_only_function

namespace std {

template<typename R, typename... Args> class __move_function_base {
public:
	__move_function_base() {}

	virtual ~__move_function_base() = default;

	virtual R operator()(Args &&...) = 0;
};

template<typename F, typename... Args>
class __move_func final: public __move_function_base<std::invoke_result_t<F, Args...>, Args...> {
	using Result_type = std::invoke_result_t<F, Args...>;
	F _f;

public:
	__move_func(F && f) noexcept(std::is_nothrow_move_constructible_v<F>): _f(std::move(f)) {}

	Result_type operator()(Args &&... args) override { return _f(std::move(args)...); }
};

template<class...> class move_only_function;

template<typename R, typename... Args> class move_only_function<R(Args...)> {
	std::unique_ptr<__move_function_base<R, Args...>> _f;

public:
	template<typename F,
	  typename = std::enable_if_t<std::is_invocable_r_v<R, std::remove_reference_t<F>, Args...>>>
	move_only_function(F &&) noexcept(std::is_nothrow_move_constructible_v<F>);

	move_only_function() noexcept;
	move_only_function(move_only_function &&) noexcept;
	move_only_function(move_only_function const &) = delete;

	move_only_function & operator=(move_only_function &&) noexcept;
	move_only_function & operator=(move_only_function const &) = delete;

	R operator()(Args &&...) const;

	operator bool() const noexcept;
}; //// move_only_function

//
// ----- move_only_function -----
//
template<typename R, typename... Args>
move_only_function<R(Args...)>::move_only_function() noexcept: _f(nullptr) {}

template<typename R, typename... Args>
move_only_function<R(Args...)>::move_only_function(move_only_function && mof) noexcept
	: _f(std::move(mof._f)) {}

template<typename R, typename... Args>
template<typename F, typename>
move_only_function<R(Args...)>::move_only_function(F && f) noexcept(
  std::is_nothrow_move_constructible_v<F>)
	: _f(std::make_unique<__move_func<std::remove_reference_t<F>, Args...>>(std::move(f))) {}

template<typename R, typename... Args>
move_only_function<R(Args...)> & move_only_function<R(Args...)>::operator=(
  move_only_function && mof) noexcept {
	_f = std::move(mof._f);
	return *this;
}

template<typename R, typename... Args>
R move_only_function<R(Args...)>::operator()(Args &&... args) const {
	if((bool)_f)
		return _f->operator()(std::move(args)...);
	throw std::bad_function_call();
}

template<typename R, typename... Args>
move_only_function<R(Args...)>::operator bool() const noexcept {
	return (bool)_f;
}

} // namespace std

#define __cpp_lib_move_only_function 20220404L
#endif
