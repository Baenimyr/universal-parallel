#pragma once

#include "universal/parallel/api.h"

#include <memory>

namespace universal::parallel {

class CompletableFuture_data;

/** \brief Structure contenant l’action à exécuter pour générer une valeur.
 *
 * \ref _cible est un pointeur vers la structure de donnée du CompletableFuture.
 * C’est cette structure qui doit être remplie et activée pour notifier les éléments dépendants.
 * Ce pointeur est remplie systématiquement quand un \ref CompletableFuture_::CompletableFuture_(action_t &&) est crée à partir d’une action.
 */
class UNI_PARALLEL_API IAction {
public:
	std::weak_ptr<CompletableFuture_data> _cible;

public:
	IAction() = default;

	IAction(std::weak_ptr<CompletableFuture_data> c): _cible(c) {}

	virtual ~IAction() noexcept = default;

	/** \brief Teste si les pré-conditions sont réunies.
	 *
	 * Cette fonction est utilisée pour vérifier si l’action est prête à être exécutée sans bloquer en attente d’un autre CompletableFuture.
	 * Elle est interrogée avant le déclenchement de \ref run().
	 */
	virtual bool pret() const = 0;

	/** \brief Exécute la commande. */
	virtual void run() = 0;
}; //// IAction

} // namespace universal::parallel
