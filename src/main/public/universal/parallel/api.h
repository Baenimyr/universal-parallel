#pragma once
// Nouvelle syntaxe des attributs
#if __cplusplus >= 202002L && __cpp_namespace_attributes >= 201411L
	#if __linux__ && __has_cpp_attribute(gnu::visibility)
		#define API_EXPORT_ATTR gnu::visibility("default")
		#define API_IMPORT_ATTR gnu::visibility("default")
		#define LOCAL_ATTR gnu::visibility("hidden")
	#elif _WIN32 && __has_cpp_attribute(gnu::dllexport) && __has_cpp_attribute(gnu::dllimport)
		#define API_EXPORT_ATTR gnu::dllexport
		#define API_IMPORT_ATTR gnu::dllimport
		#define LOCAL_ATTR
	#endif
#endif

// Macros génériques pour l’importation, exportation et privatisation
#ifndef API_EXPORT
	#if defined API_EXPORT_ATTR && defined API_IMPORT_ATTR && defined LOCAL_ATTR
		#define API_EXPORT [[API_EXPORT_ATTR]]
		#define API_IMPORT [[API_IMPORT_ATTR]]
		#define API_LOCAL [[LOCAL_ATTR]]
	#elif __linux__
		#define API_EXPORT __attribute__((visibility("default")))
		#define API_IMPORT __attribute__((visibility("default")))
		#define API_LOCAL __attribute__((visibility("hidden")))
	#elif _WIN32
		#define API_EXPORT __declspec(dllexport)
		#define API_IMPORT __declspec(dllimport)
		#define API_LOCAL
	#else
		#error "Plateforme inconnue."
	#endif
#endif

// Macros pour le projet actuel (UNI_PARALLEL_*)
#ifndef UNI_PARALLEL_API
	#if parallel_STATIC
		#define UNI_PARALLEL_API
		#define UNI_PARALLEL_LOCAL
	#elif parallel_EXPORTS
		#define UNI_PARALLEL_API API_EXPORT
	#else
		#define UNI_PARALLEL_API API_IMPORT
	#endif
	#define UNI_PARALLEL_LOCAL API_LOCAL
#endif
