#pragma once
#include "executor.hpp"
#include "universal/parallel/api.h"
#include "universal/parallel/action.hpp"

#include <cassert>
#include <chrono>
#include <condition_variable>
#include <exception> // std::exception_ptr
#include <list>
#include <memory> // std::shared_ptr, std::weak_ptr
#include <mutex>
#include <type_traits> // std::enable_if, std::is_base_of
#include <variant>
#include <vector>

namespace universal::parallel {

template<typename T>
concept non_null = std::negation<std::is_void<T>>::value;

class CompletableFuture_data;
class CompletableFuture_;
template<typename T> class CompletableFuture;

using ActionAttente = std::shared_ptr<IAction>;
using ActionExecution = std::weak_ptr<IAction>;
using data_t = std::shared_ptr<void>;

/** \brief Données partagées entre les \ref CompletableFuture.
 */
class UNI_PARALLEL_API CompletableFuture_data: public std::enable_shared_from_this<CompletableFuture_data> {
	friend class CompletableFuture_;
	template<typename T> friend class CompletableFuture;

private:
	/** Protège les modification des valeurs. */
	std::mutex _mutex;

	/** Permet l’attente passive de modification sur les valeurs. */
	std::condition_variable _cv;

	/** \brief Liste des \ref CompletableFuture à réveiller dès que la valeur est prête. */
	std::list<std::weak_ptr<CompletableFuture_data>> pendings;

	Executor * executor = nullptr;
	/**
	 * - ActionAttente non nulle est une action non démarrée.
	 * - ActionAttente nulle est une action annulée avant son démarrage.
	 * - ActionExecution non expirée est une action en cours.
	 * - ActionExecution vide ou expirée est une action terminée.
	 */
	std::variant<ActionAttente, ActionExecution> action;

	/**
	 * - std::monostate si annulé
	 * - std::unique_ptr<CompletableFuture_action> si action mais ni valeur ni erreur. Le pointeur sera nul si l’action est en cours
	 * - std::nested_exception en cas d’erreur
	 * - std::shared_ptr<void> est la valeur
	 */
	std::variant<std::nested_exception, // erreur
	  data_t // valeur
	  >
	  val;

public:
	CompletableFuture_data(ActionAttente);
	CompletableFuture_data(decltype(val) &&);

	/**
	 * Un \ref CompletableFuture est en attente si sa valeur est une action non-nulle.
	 * Une action nulle signifie qu’elle est en cours d’exécution ailleur.
	 * \return si la valeur est une action non démarrée.
	 */
	bool isPending() const;

public:
	/** \brief Avertit d’un changement dans les préconditions.
	 *
	 * Si elle est prête, l’action peut être démarrée.
	 */
	void notify();

	/** \brief Avertit tous les éléments en attente de this.
	 * \par Une fois notifiés, la liste est purgée.
	 */
	void _notify_all();

	void set(decltype(val) && v);
}; //// CompletableFuture_data

/** \brief Implémentation commune à \ref CompletableFuture.
 * \par Cette classe définie les fonctions indépendantes du type.
 * Elle peut être utilisée en référence pour effacer le type.
 */
class UNI_PARALLEL_API CompletableFuture_ {
protected:
	std::shared_ptr<CompletableFuture_data> _shared_data;

	/** \brief Construit avec une valeur pré-définie. */
	CompletableFuture_(data_t && valeur);

	/** \brief Construit avec une action pour le calcul différé de la valeur.
	 * \par la \ref CompletableFuture_action::_cible est assignée avec un pointeur vers les données.
	 */
	CompletableFuture_(ActionAttente && action);

	/** \brief Construit avec une erreur. */
	CompletableFuture_(std::nested_exception && ex);

	CompletableFuture_ & operator=(CompletableFuture_ const &);
	CompletableFuture_ & operator=(CompletableFuture_ &&);

public:
	CompletableFuture_(CompletableFuture_ const &);
	CompletableFuture_(CompletableFuture_ &&);

public: // lecture
	bool isPending() const;

	bool isRunning() const;

	/** \brief Vérifie si le \ref CompletableFuture est prêt. */
	bool isDone() const;

	/** \brief Interroge si le \ref CompletableFuture a été annulé.
	 * \see CompletableFuture::cancel()
	 */
	bool isCancelled() const;

	void wait() const noexcept;

	bool wait(std::chrono::milliseconds) const noexcept;

public: // modification
	/** \brief Fixe manuellement l’erreur.
	 * \return true si aucune valeur n’était disponible et aucune erreur déjà interceptée.
	 */
	bool complete_exception(std::nested_exception const &);

	/** \brief Annule l’opération.
	 *
	 * Un calcul en cours ne sera pas interrompu.
	 * Si la valeur n’est pas prête, place une erreur et marque le future comme terminé.
	 */
	bool cancel();

	void add_dependant(CompletableFuture_ const &) const;
}; //// CompletableFuture_

template<typename F, typename T, typename V>
struct combine_result: public std::invoke_result<F, typename CompletableFuture<T>::value_type,
						 typename CompletableFuture<V>::value_type> {};

template<typename F, typename V>
struct combine_result<F, void, V>
	: public std::invoke_result<F, typename CompletableFuture<V>::value_type> {};

template<typename F, typename T>
struct combine_result<F, T, void>
	: public std::invoke_result<F, typename CompletableFuture<T>::value_type> {};

template<typename F> struct combine_result<F, void, void>: public std::invoke_result<F> {};

/**
 * \brief Un CompletableFuture est une forme élaborée de std::shared_future.
 * Elle permet la combinaison chaînée de tâches qui seront exécutée à la suite.
 *
 * Quand une fonction est fournie pour générer la valeur de façon différée, cette fonction est soumise à un exécuteur externe dès que les conditions nécessaires sont réunies.
 * En particulier, si l’exécuteur utilise une file, aucun fonction qui lui est soumise n’est bloquante en attente d’une action plus loin dans la file.
 * Si la génération échoue et produit une erreur, elle sera interceptée et déclenchée à nouveau si quelqu’un tente de récupérer la valeur.
 */
template<typename T> class CompletableFuture: public CompletableFuture_ {
public:
	using value_type = T &;
	using const_value_type = T const &;

	template<typename U> friend class CompletableFuture;
	friend CompletableFuture<void> all(std::vector<CompletableFuture_>);
	friend CompletableFuture<void> any(std::vector<CompletableFuture_>);

public:
	CompletableFuture(CompletableFuture const &) = default;
	CompletableFuture(CompletableFuture &&) = default;
	CompletableFuture & operator=(CompletableFuture const &) = default;
	CompletableFuture & operator=(CompletableFuture &&) = default;

private:
	CompletableFuture() = default;

	CompletableFuture(std::shared_ptr<T> valeur): CompletableFuture_(std::move(valeur)) {}

	CompletableFuture(ActionAttente action): CompletableFuture_(std::move(action)) {}

	CompletableFuture(std::nested_exception ex): CompletableFuture_(std::move(ex)) {}

public: // lecture
	/** \brief Accède à la valeur ou bloque jusqu’à ce qu’elle soit prête.
	 *
	 * Peut retourner nullptr si ce \ref CompletableFuture ne produit aucune valeur.
	 */
	value_type get();
	const_value_type get() const;

	/** \brief Accède à la valeur ou bloque pendant un certain temps.
	 *
	 * \return nullptr si la valeur n’est pas prête ou si aucune valeur n’est produite.
	 */
	value_type get(std::chrono::milliseconds);
	const_value_type get(std::chrono::milliseconds) const;

	/** \brief Récupère un pointeur vers la valeur produite ou un pointeur vers la valeur par défaut. */
	value_type get(value_type);
	const_value_type get(value_type) const;

public: // modification
	/** \brief Fixe manuellement la valeur.
	 * \return true si la valeur a bien été placée.
	 */
	template<typename... Args> bool complete(Args &&... args);

	/** \brief Fixe manuellement la valeur après le temps imparti. */
	template<typename... Args> bool complete(std::chrono::milliseconds, Args &&... args);

public: // création
	/** \brief Crée un CompletableFuture pré-rempli. */
	template<typename... Args> static CompletableFuture completed(Args &&... args);

	/** \brief Crée un CompletableFuture pré-rempli avec une erreur. */
	static CompletableFuture failed(std::nested_exception);

	/** \brief Crée un CompletableFuture dont la valeur sera calculé.
	 *
	 * La fonction génératrice est immédiatement soumise à l’exécuteur.
	 * Cette fonction marque souvent le point de départ d’une chaîne de tâches.
	 */
	template<typename F> static CompletableFuture supply(F && fonction, Executor * = nullptr);

public: // chaînage
	template<typename F>
	CompletableFuture<std::invoke_result_t<F, value_type, std::exception_ptr>> handle(F &&,
	  Executor * = nullptr) const;

	/** \brief Ajoute une fonction intermédiaire qui servira à produire une valeur de remplacement dans le cas d’une erreur précédemment. */
	template<typename F> CompletableFuture<T> except(F &&, Executor * = nullptr) const;

	/** \brief Action qui sera exécutée avec la valeur produite.
	 *
	 * Cette action différée ne produit aucune valeur.
	 */
	template<typename F>
	CompletableFuture<void> thenAccept(F && fonction, Executor * = nullptr) const;

	/** \brief Action qui sera exécutée avec les valeurs de 2 \ref CompletableFuture.
	 *
	 * Cette action différée ne produit aucune valeur.
	 */
	template<typename U, typename F>
	CompletableFuture<void> thenAcceptBoth(CompletableFuture<U>, F &&, Executor * = nullptr) const;

	template<typename F>
	CompletableFuture<std::invoke_result_t<F, value_type>> thenApply(F &&,
	  Executor * = nullptr) const;

	template<typename Type2, typename Function>
	CompletableFuture<typename combine_result<Function, T, Type2>::type> thenCombine(
	  CompletableFuture<Type2>, Function &&, Executor * = nullptr) const;

	/**
	 * \brief Exécute une fonction dès que le future est prêt.
	 *
	 * La fonction n’utilisera pas la valeur produite.
	 */
	template<typename F> CompletableFuture<void> thenRun(F &&, Executor * = nullptr) const;

}; //// CompletableFuture

template<> class UNI_PARALLEL_API CompletableFuture<void>: public CompletableFuture_ {
	template<typename T> friend class CompletableFuture;
	friend CompletableFuture<void> all(std::vector<CompletableFuture_>);
	friend CompletableFuture<void> any(std::vector<CompletableFuture_>);

public:
	using value_type = void;
	using const_value_type = void;

public:
	CompletableFuture(CompletableFuture const &) = default;
	CompletableFuture(CompletableFuture &&) = default;
	CompletableFuture & operator=(CompletableFuture const &) = default;
	CompletableFuture & operator=(CompletableFuture &&) = default;

private:
	CompletableFuture();

	CompletableFuture(ActionAttente action): CompletableFuture_(std::move(action)) {}

	CompletableFuture(std::nested_exception ex): CompletableFuture_(std::move(ex)) {}

public: // modification
	/** \brief Fixe manuellement la valeur.
	 * \return true si la valeur a bien été placée.
	 */
	bool complete();

	/** \brief Fixe manuellement la valeur après le temps imparti. */
	bool complete(std::chrono::milliseconds);

public: // création
	/** \brief Crée un CompletableFuture pré-rempli. */
	static CompletableFuture completed();

	/** \brief Crée un CompletableFuture pré-rempli avec une erreur. */
	static CompletableFuture failed(std::nested_exception);

	/** \brief Exécute une fonction en différé. */
	template<typename F> static CompletableFuture supply(F && fonction, Executor * = nullptr);

public: // chaînage
	template<typename F,
	  typename = std::enable_if_t<std::is_invocable_v<F, void, std::exception_ptr>>>
	CompletableFuture<std::invoke_result_t<F, void, std::exception_ptr>> handle(F &&,
	  Executor * = nullptr) const;

	/** \brief Ajoute une fonction intermédiaire qui servira à produire une valeur de remplacement dans le cas d’une erreur précédemment. */
	template<typename F> CompletableFuture<void> except(F &&, Executor * = nullptr) const;

	template<typename Type2, typename Function>
	CompletableFuture<typename combine_result<Function, void, Type2>::type> thenCombine(
	  CompletableFuture<Type2>, Function &&, Executor * = nullptr) const;

	/**
	 * \brief Exécute une fonction dès après la fonction du CompletableFuture.
	 */
	template<typename F> CompletableFuture<void> thenRun(F &&, Executor * = nullptr) const;
}; //// CompletableFuture<void>

/** \brief Crée un CompletableFuture qui sera prêt dès que tous les CompletableFutures seront prêts. */
UNI_PARALLEL_API CompletableFuture<void> all(std::vector<CompletableFuture_>);

/** \brief Crée un CompletableFuture qui sera prêt dès que tous les CompletableFutures seront prêts. */
UNI_PARALLEL_API CompletableFuture<void> any(std::vector<CompletableFuture_>);

//
//
//
template<typename T> typename CompletableFuture<T>::value_type CompletableFuture<T>::get() {
	struct {
		value_type operator()(data_t & d) const { return *static_cast<T *>(d.get()); }

		value_type operator()(std::nested_exception & ne) const { ne.rethrow_nested(); }
	} static get;

	wait();
	return std::visit(get, this->_shared_data->val);
} //// CompletableFuture<T>::get()

template<typename T>
typename CompletableFuture<T>::const_value_type CompletableFuture<T>::get() const {
	struct {
		const_value_type operator()(data_t const & d) const { return *static_cast<T *>(d.get()); }

		const_value_type operator()(std::nested_exception const & ne) const { ne.rethrow_nested(); }
	} static get;

	wait();
	return std::visit(get, this->_shared_data->val);
} //// CompletableFuture<T>::get() const

template<typename T>
typename CompletableFuture<T>::value_type CompletableFuture<T>::get(
  std::chrono::milliseconds timeout) {
	return wait(timeout) ? get() : nullptr;
} //// CompletableFuture<T>::get(std::chrono::milliseconds)

template<typename T>
typename CompletableFuture<T>::const_value_type CompletableFuture<T>::get(
  std::chrono::milliseconds timeout) const {
	return wait(timeout) ? get() : nullptr;
} //// CompletableFuture<T>::get(std::chrono::milliseconds) const

template<typename T>
typename CompletableFuture<T>::value_type CompletableFuture<T>::get(
  CompletableFuture<T>::value_type def) {
	if(!isDone()) {
		return def;
	}
	return get();
} //// CompletableFuture<T>::get(CompletableFuture<T>::value_type)

template<typename T>
typename CompletableFuture<T>::const_value_type CompletableFuture<T>::get(
  CompletableFuture<T>::value_type def) const {
	if(!isDone()) {
		return def;
	}
	return get();
} //// CompletableFuture<T>::get(CompletableFuture<T>::value_type) const

template<typename T>
template<typename... Args>
bool CompletableFuture<T>::complete(Args &&... args) {
	if(!_shared_data->isPending())
		return false;
	{
		std::unique_lock lg(_shared_data->_mutex);
		if(!_shared_data->isPending())
			return false;
		auto val_ptr = std::make_shared<T>(std::forward<Args>(args)...);
		_shared_data->set(std::move(val_ptr));
	}
	_shared_data->_notify_all();
	return true;
} //// CompletableFuture<T>::complete(Args &&)

template<typename T>
template<typename... Args>
bool CompletableFuture<T>::complete(std::chrono::milliseconds timeout, Args &&... args) {
	if(!_shared_data->isPending())
		return false;
	{
		std::unique_lock lg(_shared_data->_mutex);
		if(!_shared_data->isPending())
			return false;
		_shared_data->_cv.wait_for(lg, timeout);
		if(!_shared_data->isPending())
			return false;
		_shared_data->set(std::move(std::make_shared<T>(std::forward<Args>(args)...)));
	}
	_shared_data->_notify_all();
	return true;
} //// CompletableFuture<T>::complete(std::chrono::milliseconds, Args &&)

template<typename T>
template<typename... Args>
CompletableFuture<T> CompletableFuture<T>::completed(Args &&... args) {
	return CompletableFuture<T>(std::make_shared<T>(std::forward<Args>(args)...));
} //// CompletableFuture<T>::completed(Args &&)

template<typename T> CompletableFuture<T> CompletableFuture<T>::failed(std::nested_exception ex) {
	return CompletableFuture<T>(std::move(ex));
}

template<typename T>
template<typename Fonction>
CompletableFuture<T> CompletableFuture<T>::supply(Fonction && f, Executor * executor) {
	static_assert(std::is_invocable_r_v<T, Fonction>);

	struct supply_action final: IAction {
		std::decay_t<Fonction> _action;

		supply_action(std::decay_t<Fonction> && f): _action(std::move(f)) {}

		bool pret() const override { return true; }

		void run() override {
			if(auto cible = _cible.lock()) {
				cible->set(std::make_shared<T>(_action()));
				cible->_notify_all();
			}
		}
	};

	CompletableFuture<T> c(ActionAttente(std::make_shared<supply_action>(std::move(f))));
	c._shared_data->executor = executor;
	c._shared_data->notify();
	return c;
} //// CompletableFuture<T>::supply

template<typename Fonction>
CompletableFuture<void> CompletableFuture<void>::supply(Fonction && f, Executor * executor) {
	static_assert(std::is_invocable_r_v<void, Fonction>);

	struct supply_action final: IAction {
		std::decay_t<Fonction> _action;

		supply_action(std::decay_t<Fonction> && f): _action(std::move(f)) {}

		bool pret() const override { return true; }

		void run() override {
			if(auto cible = _cible.lock()) {
				_action();
				cible->set(data_t());
				cible->_notify_all();
			}
		}
	};

	CompletableFuture<void> c(ActionAttente(std::make_shared<supply_action>(std::move(f))));
	c._shared_data->executor = executor;
	c._shared_data->notify();
	return c;
} //// CompletableFuture<void>::supply

// -----
template<typename T>
template<typename Fonction>
CompletableFuture<void> CompletableFuture<T>::thenAccept(Fonction && f, Executor * executor) const {
	static_assert(std::is_invocable_r_v<void, Fonction, value_type>);

	struct accept_action final: IAction {
		CompletableFuture<T> const _1;
		std::decay_t<Fonction> _action;

		accept_action(CompletableFuture<T> const & _1, std::decay_t<Fonction> && a)
			: _1(_1), _action(std::move(a)) {}

		bool pret() const override { return _1.isDone(); }

		void run() override {
			if(auto cible = _cible.lock()) {
				_action(_1.get());
				cible->set(data_t());
				cible->_notify_all();
			}
		}
	}; //// accept_action

	CompletableFuture<void> c(ActionAttente(std::make_shared<accept_action>(*this, std::move(f))));
	c._shared_data->executor = executor;
	add_dependant(c);
	return c;
} //// CompletableFuture<T>::thenAccept

template<typename T>
template<typename U, typename Fonction>
CompletableFuture<void> CompletableFuture<T>::thenAcceptBoth(CompletableFuture<U> _2_arg,
  Fonction && f, Executor * executor) const {
	static_assert(
	  std::is_invocable_r_v<void, Fonction, value_type, typename CompletableFuture<U>::value_type>);

	struct accept_both_action final: IAction {
		CompletableFuture<T> const _1;
		CompletableFuture<U> const _2;
		std::decay_t<Fonction> _action;

		accept_both_action(CompletableFuture<T> const & _1, CompletableFuture<T> const & _2,
		  std::decay_t<Fonction> a)
			: _1(_1), _2(_2), _action(std::move(a)) {}

		bool pret() const override { return _1.isDone(); }

		void run() override {
			if(auto cible = _cible.lock()) {
				_action(_1.get(), _2.get());
				cible->set(data_t());
				cible->_notify_all();
			}
		}
	}; //// accept_both_action

	CompletableFuture<void> c(
	  ActionAttente(std::make_shared<accept_both_action>(*this, _2_arg, std::move(f))));
	c._shared_data->executor = executor;
	add_dependant(c);
	_2_arg.add_dependant(c);
	return c;
} //// CompletableFuture<T>::thenAcceptBoth

template<typename T>
template<typename V, typename Fonction>
CompletableFuture<typename combine_result<Fonction, T, V>::type> CompletableFuture<T>::thenCombine(
  CompletableFuture<V> _2, Fonction && f, Executor * executor) const {
	static_assert(
	  std::is_invocable_v<Fonction, value_type, typename CompletableFuture<V>::value_type>);
	using R = typename combine_result<Fonction, T, V>::type;

	struct combine final: IAction {
		CompletableFuture<T> _1;
		CompletableFuture<V> _2;
		std::decay_t<Fonction> _action;

		combine(CompletableFuture<T> _1, CompletableFuture<V> _2, std::decay_t<Fonction> && action)
			: _1(std::move(_1)), _2(std::move(_2)), _action(std::move(action)) {}

		bool pret() const override { return _1.isDone() && _2.isDone(); }

		void run() override {
			if(auto cible = _cible.lock()) {
				if constexpr(std::is_void<V>::value) {
					cible->set(std::make_shared<R>(_action(_1.get())));
				} else {
					cible->set(std::make_shared<R>(_action(_1.get(), _2.get())));
				}
				cible->_notify_all();
			}
		}
	};

	CompletableFuture<R> c(ActionAttente(std::make_shared<combine>(*this, _2, std::move(f))));
	c._shared_data->executor = executor;
	add_dependant(c);
	_2.add_dependant(c);
	return c;
} //// CompletableFuture<T>::thenCombine

template<typename V, typename Fonction>
CompletableFuture<typename combine_result<Fonction, void, V>::type>
CompletableFuture<void>::thenCombine(CompletableFuture<V> _2, Fonction && f,
  Executor * executor) const {
	static_assert(std::is_void<V>::value
					? std::is_invocable_v<Fonction>
					: std::is_invocable_v<Fonction, typename CompletableFuture<V>::value_type>);
	using R = typename combine_result<Fonction, void, V>::type;

	struct combine final: IAction {
		CompletableFuture<void> _1;
		CompletableFuture<V> _2;
		std::decay_t<Fonction> _action;

		combine(CompletableFuture<void> _1, CompletableFuture<V> _2,
		  std::decay_t<Fonction> && action)
			: _1(std::move(_1)), _2(std::move(_2)), _action(std::move(action)) {}

		bool pret() const override { return _1.isDone() && _2.isDone(); }

		void run() override {
			if(auto cible = _cible.lock()) {
				if constexpr(std::is_void<V>::value) {
					_action();
				} else {
					_action(_2.get());
				}
				cible->set(data_t());
				cible->_notify_all();
			}
		}
	};

	CompletableFuture<R> c(ActionAttente(std::make_shared<combine>(*this, _2, std::move(f))));
	c._shared_data->executor = executor;
	add_dependant(c);
	_2.add_dependant(c);
	return c;
} //// CompletableFuture<T>::thenCombine

template<typename T>
template<typename Fonction>
CompletableFuture<void> CompletableFuture<T>::thenRun(Fonction && f, Executor * executor) const {
	static_assert(std::is_invocable_v<Fonction>);

	struct run_action final: public IAction {
		CompletableFuture<T> _1;
		std::decay_t<Fonction> _action;

		run_action(CompletableFuture<T> _prev, std::decay_t<Fonction> && f)
			: _1(_prev), _action(std::move(f)) {}

		bool pret() const override { return _1.isDone(); }

		void run() override {
			if(auto cible = _cible.lock()) {
				_action();
				cible->set(data_t{});
				cible->_notify_all();
			}
		}
	}; //// run_action

	CompletableFuture<void> c(ActionAttente(std::make_shared<run_action>(*this, std::move(f))));
	c._shared_data->executor = executor;
	add_dependant(c);
	return c;
} //// CompletableFuture<T>::thenRun

template<typename Fonction>
CompletableFuture<void> CompletableFuture<void>::thenRun(Fonction && f, Executor * executor) const {
	static_assert(std::is_invocable_v<Fonction>);

	struct run_action final: public IAction {
		CompletableFuture<void> _1;
		std::decay_t<Fonction> _action;

		run_action(CompletableFuture<void> _prev, std::decay_t<Fonction> && f)
			: _1(_prev), _action(std::move(f)) {}

		bool pret() const override { return _1.isDone(); }

		void run() override {
			if(auto cible = _cible.lock()) {
				_action();
				cible->set(data_t{});
				cible->_notify_all();
			}
		}
	}; //// run_action

	CompletableFuture<void> c(ActionAttente(std::make_shared<run_action>(*this, std::move(f))));
	c._shared_data->executor = executor;
	add_dependant(c);
	return c;
} //// CompletableFuture<void>::thenRun

} // namespace universal::parallel
