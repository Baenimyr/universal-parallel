#pragma once
#include <atomic>
#include <chrono> // std::chrono::milliseconds
#include <memory> // std::unique_ptr
#include <mutex>
#include <optional>
#include <semaphore>

#include <cassert>
#ifndef _NDEBUG
#include <thread>
#define DEBUG_YIELD std::this_thread::yield();
#else
#define DEBUG_YIELD
#endif

#include "conteneur.hpp"

namespace universal::parallel {

/** \brief Une file optimisée pour l’ajout et suppression en concurrence.
 *
 * La file est une liste chaînée.
 * Deux sémaphore assurent le non dépassement de la capacité et l’attente passive quand la liste est vide.
 *
 * L’ajout se fait atomiquement en modifiant \link concurrent_queue_node::next \endlink vers le nouveau nœud.
 *
 * ## Suppression.
 * La suppression se fait en redirigeant \link FileBloquante::tete \endlink vers le deuxième nœud.
 * Le nœud de tête ne contient jamais de valeur et la valeur est récupérée dans le nouveau nœud de tête.
 */
template<typename T>
class FileBloquante final :
	public ConteneurConcurrent<T>
{

struct concurrent_queue_node;
using node_ptr = concurrent_queue_node *;
using atomic_node = std::atomic<node_ptr>;

/** \brief Un nœud de la liste chaînée.
 *
 * Le dernier nœud de la file pointe toujours vers nullptr.
 * Un nœud ne doit pas être supprimé tant que 'val' n’est pas récupéré.
 * Le processus qui en a lu la valeur a copié le pointeur et est responsable de sa désallocation.
 *
 * \see https://en.wikipedia.org/wiki/Non-blocking_linked_list#cite_note-2
 */
struct concurrent_queue_node {
	atomic_node next;
	std::atomic<T *> _val;

	constexpr concurrent_queue_node(std::nullptr_t val):
		next(nullptr),
		_val(val) {}

	template<typename ...Args>
	constexpr concurrent_queue_node(Args &&... args):
		next(nullptr),
		_val(new T(args...)) {}

	constexpr bool final() const { return next == nullptr; }
};



protected:
	std::counting_semaphore<> _taille;
	std::counting_semaphore<> _capacite;
	std::mutex _pop_mutex;

	/** \brief Pointeur vers le premier élément de la file. */
	atomic_node tete;
	/** \brief Pointeur vers le possiblement le dernier élément de la file. */
	atomic_node fin;

	/** \brief Supprime le nœud de tête.
	 *
	 * Cette opération est atomique.
	 * \return le pointeur qui été stocké.
	 */
	std::unique_ptr<T> pop_();

	/** \brief Chaîne le nouveau nœud à la suite.
	 *
	 * Cette opération est atomique et ne nécessite pas de synchronisation.
	 */
	void push_(node_ptr const);

public:
	/** \brief Initialise une file concurrente.
	 *
	 * Bloque l’ajout si la file est pleine.
	 */
	FileBloquante(size_t capacite = 128);
	FileBloquante(FileBloquante const&) = delete;
	FileBloquante(FileBloquante &&) = delete;
	~FileBloquante();

	/** \return true si la file est vide. Peut ne pas être à jour. */
	bool empty() const noexcept override { return this->tete.load()->final(); }

	/** \brief Ajoute un élément dans la liste et bloque si elle est pleine. */
	template<typename ...Args>
	void push(Args &&...);

	void push(T &&) override;

	/** \brief Ajoute un élément dans la file et retourne en cas d’échec. */
	bool try_push(T const&, std::chrono::milliseconds = std::chrono::milliseconds(0)) override;

	T pop() override;
	std::optional<T> try_pop(std::chrono::milliseconds = std::chrono::milliseconds(0)) override;
}; //// FileBloquante


template<typename T>
FileBloquante<T>::FileBloquante(
	size_t capacite
):
	_taille(0),
	_capacite(capacite),
	tete(new concurrent_queue_node(nullptr)),
	fin(tete.load())
{}

template<typename T>
FileBloquante<T>::~FileBloquante() {
	node_ptr node = this->tete;
	while (node->next != nullptr) {
		delete node->next.load()->_val;
		node_ptr node_n = node->next;
		delete node;
		node = node_n;
	}
	delete node;
}



// Michael & Scott queue algorithm
template<typename T>
std::unique_ptr<T> FileBloquante<T>::pop_() {
	T * tmp_val_ptr;
	node_ptr tete, fin, suivant;

	while (true) {
		std::lock_guard r_w_lock(this->_pop_mutex);
		tete = this->tete.load();
		fin = this->fin.load();
		suivant = tete->next.load();

		DEBUG_YIELD

		if (tete == this->tete.load()) {
			assert(suivant != nullptr);
			if (tete == fin) {
				// possible erreur de fin
				this->fin.compare_exchange_weak(fin, suivant);
			}
			// Recule le pointeur tete d’un cran
			else if (this->tete.compare_exchange_weak(/*ancien=*/tete, /*nouveau=*/suivant)) {
				tmp_val_ptr = suivant->_val.load();
				assert(tmp_val_ptr != nullptr);
				// Marque le nœud prêt à être supprimé
				suivant->_val.store(nullptr);
				suivant->_val.notify_one();
				break;
			}
		}
	}

	/* Le nœud précédant peut être supprimé librement car sa valeur est gérée
	par le processus qui à acquit le droit de le lire */
	auto old = tete->_val.load();
	if (old != nullptr)
		// Attend que le nœud soit prêt à être supprimé
		tete->_val.wait(old);
	delete tete;
	return std::unique_ptr<T>(tmp_val_ptr);
} //// FileBloquante::pop_()


template<typename T>
void FileBloquante<T>::push_(node_ptr const nouveau) {
	while (true) {
		node_ptr dernier = this->fin.load();
		node_ptr suivant = dernier->next.load();

		DEBUG_YIELD

		if (dernier == this->fin.load()) {
			if (suivant != nullptr)
				// Déplace le pointeur fin s’il est mal positionné
				this->fin.compare_exchange_weak(dernier, suivant);
			else
			// Chaîne un nouvel élément à la fin de la liste
			if (dernier->next.compare_exchange_weak(suivant, nouveau)) {
				assert(dernier->next.load() != nullptr);
				// Déplace le pointeur de fin, non obligatoire
				/** Le pointeur `fin` est toujours positionné sur cette valeur ou une valeur suivante.
				Car l’insertion a été réalisée sur l’ancienne position `fin` connue,
				soit la nouvelle attribution a réussie soit un autre processus l’a déplacé pendant `suivant != nullptr`.*/
				this->fin.compare_exchange_weak(dernier, nouveau);
				assert(this->tete.load()->next.load() != nullptr);
				return;
			}
		}
	}
} //// FileBloquante::push_(node_ptr const)


template<typename T>
T FileBloquante<T>::pop() {
	// Bloque jusqu’à ce qu’un élément soit disponible
	this->_taille.acquire();
	std::unique_ptr<T> valeur_ptr = this->pop_();
	this->_capacite.release();
	return std::move(*valeur_ptr);
} //// FileBloquante::pop()


template<typename T>
std::optional<T> FileBloquante<T>::try_pop(std::chrono::milliseconds timeout) {
	if (this->_taille.try_acquire_for(timeout)) {
		// assure qu’un élément est disponible
		std::unique_ptr<T> valeur_ptr = this->pop_();
		this->_capacite.release();
		return std::make_optional<T>(std::move(*valeur_ptr));
	} else return std::nullopt;
} //// FileBloquante::try_pop


template<typename T>
template<typename ...Args>
void FileBloquante<T>::push(Args &&... args) {
	this->_capacite.acquire();
	push_(new concurrent_queue_node(std::move(args)...));
	this->_taille.release();
} //// FileBloquante::push

template<typename T>
void FileBloquante<T>::push(T && v) {
	this->_capacite.acquire();
	push_(new concurrent_queue_node(std::move(v)));
	this->_taille.release();
} //// FileBloquante::push

template<typename T>
bool FileBloquante<T>::try_push(T const& val, std::chrono::milliseconds timeout) {
	if (this->_capacite.try_acquire_for(timeout)) {
		push_(new concurrent_queue_node(val));
		this->_taille.release();
		return true;
	} else return false;
} //// FileBloquante::try_push

} //// namespace universal::parallel