#pragma once
#include <chrono> // std::chrono::milliseconds
#include <limits>
#include <optional> // std::optional<T>
#include <type_traits>

namespace universal::parallel {

/** \brief Interface d’une file ou pile concurrente.
 *
 * Cette file permet l’insertion et l’extraction de nouveaux éléments en parallèle de façon optimale.
 * L’ordre de sortie de la file n’est pas certifié être le même que l’ordre d’insertion.
 */
template<typename T>
struct ConteneurConcurrent {
	static_assert(std::is_copy_constructible_v<T> || std::is_move_constructible_v<T>);
	static_assert(std::is_same_v<T, std::remove_reference_t<T>>);

	virtual ~ConteneurConcurrent() = default;

	virtual bool empty() const noexcept = 0;

	/** Indique la limite de capacité de cette file.
	 * Si la limite est atteinte, d’autres fonctionse peuvent agir différemment.
	 */
	virtual size_t max_size() const noexcept { return std::numeric_limits<size_t>::max(); }

	virtual void push(T &&) = 0;

	/** \brief Tente d’ajouter un nouvel élément.
	 *
	 * L’ajout d’un nouvel élément peut échouer si la capacité de la file ne le permet pas.
	 * \return true en cas d’insertion réussie
	 */
	virtual bool try_push(T const&, std::chrono::milliseconds timeout = std::chrono::milliseconds(0)) = 0;

	/** \brief Retire un élément de la file. */
	virtual T pop() = 0;

	/** \brief Tente de retirer un élément de la file.
	 *
	 * Cette opération peut échouer si la file est vide.
	 */
	virtual std::optional<T> try_pop(std::chrono::milliseconds timeout = std::chrono::milliseconds(0)) = 0;
}; //// ConteneurConcurrent

} //// namespace universal::parallel