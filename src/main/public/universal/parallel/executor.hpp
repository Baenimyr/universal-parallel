#pragma once
#include "universal/move_function.hpp"
#include "universal/parallel/api.h"

#include <chrono>
#include <functional>
#include <type_traits> // std::result_of

namespace universal::parallel {

struct UNI_PARALLEL_API Executor {
	/**
	 * Un exécuteur ne doit pas être détruit tant que des exécutions asynchrones ont lieu.
	 */
	virtual ~Executor() noexcept = default;

	/**
	 * \brief Prépare une fonction pour son exécution asynchrone.
	 *
	 * La fonction sera exécutée avec les paramètres fournis.
	 * Elle est equivalente à execute(bind_front(f)).
	 * \param f fonction, peut être une expression lambda
	 * \param arg premier argument sinon utilisez \ref execute
	 * \param args reste des arguments à passer à la fonction
	 */
	template<class F, typename Arg, typename... Args,
	  typename = std::enable_if_t<std::is_invocable_r_v<void, F, Arg, Args...>>>
	void execute_args(F && f, Arg && arg_1, Args &&... args) noexcept(
	  std::is_nothrow_move_constructible_v<F>);

	/**
	 * \brief Exécute la fonction de manière asynchrone.
	 *
	 * L’exécuteur ne garantit pas l’ordre d’exécution pour plusieurs fonctions soumises en parallèle ou séquentiellement.
	 * Cette fonction supporte les appels concurrents.
	 */
	virtual void execute(std::move_only_function<void(void)> &&) = 0;
}; //// Executor

/** \brief L’ExecutorService est un exécuteur sous la forme d’un objet et de processus gérés.
 *
 * En tant que service, cet exécuteur n’est pas toujours disponible. Il peut être arrêté et il devient alors
 * impossible de lui soumettre de nouvelles opérations.
 * Dans tous les cas, il est impossible d’interrompre une opération en cours.
 *
 * La fonction \ref ExecutorService::shutdown() permet un arrêt en douceur.
 * Immédiatement après son appel, il est impossible d’ajouter de nouvelles opérations à la file d’attente.
 * La fonction bloque jusqu’à ce que les opérations en cours et en attente soient terminées.
 *
 * La fonction \ref ExecutorService::shutdown(std::vector<std::function<void(void)>> &) accélère la fermeture.
 * Les opérations déjà en cours doivent toujours être terminées pour que la fonction retourne.
 * Si ce service dispose d’une file d’attente, tous les éléments de la file d’attente sont transférés dans la liste.
 */
struct UNI_PARALLEL_API ExecutorService: public Executor {
	virtual ~ExecutorService() noexcept = default;

	/** \return true si le service est en cours d’arrêt mais des sous-processus sont encore en cours.
	 */
	virtual bool is_shutdown() const noexcept = 0;

	/** \return true si le service est totalement à l’arrêt. */
	virtual bool is_terminated() const noexcept = 0;

	/** \brief Demande l’interruption du service.
	 * \par La fonction retourne immédiatement mais l’\ref Exector n’acceptera plus de nouvelle tâches.
	 */
	virtual void shutdown() = 0;

	virtual void shutdown(std::vector<std::function<void(void)>> &) = 0;

	/** \brief Attend que le service soit totalement arrêté. */
	virtual void wait_terminaison(std::chrono::milliseconds) = 0;
}; //// ExecutorService

template<class F, typename Arg, typename... Args, typename>
void Executor::execute_args(F && fonction, Arg && arg_1, Args &&... args) noexcept(
  std::is_nothrow_move_constructible_v<F>) {
	auto b = std::bind_front<std::remove_reference<F>::type>(std::move(fonction),
	  std::move(arg_1),
	  std::forward(args)...);
	static_assert(std::is_invocable_r_v<void, decltype(b)>);
	execute(std::move_only_function<void()>(std::move(b)));
} //// Executor::execute_args

} // namespace universal::parallel
