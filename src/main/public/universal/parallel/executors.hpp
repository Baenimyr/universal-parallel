/** \brief Ensemble d’exécuteurs possibles
 */

#pragma once
#include "universal/move_function.hpp"
#include "universal/parallel/api.h"
#include "universal/parallel/executor.hpp"

#include <list>
#include <mutex>

namespace universal::parallel {

/** \brief Cette implémentation exécute la tâche immédiatement. */
class UNI_PARALLEL_API ExecuteurImmediat final: public Executor {
public:
	ExecuteurImmediat() {}

	ExecuteurImmediat(ExecuteurImmediat const &) = delete;
	ExecuteurImmediat(ExecuteurImmediat &&) = delete;
	~ExecuteurImmediat() = default;

	ExecuteurImmediat & operator=(ExecuteurImmediat const &) = delete;
	ExecuteurImmediat & operator=(ExecuteurImmediat &&) = delete;

	/** \brief Exécute la fonction immédiatement.
	 *
	 * Cette fonction bloque jusqu’à ce que la tâche soit complétée.
	 */
	void execute(std::move_only_function<void(void)> &&) noexcept override;
}; //// class ExecuteurImmediat

/** \brief Exécuteur ne disposant pas des ressources pour l’exécution.
 *
 * Cette exécuteur sauvegarde la liste des tâches à effectuer mais a besoin qu’un processus lui prête ses ressources pour fonctionner.
 * La fonction run permet à l’exécuteur de réaliser une tâche avant de rendre la main.
 */
class UNI_PARALLEL_API ExecuteurOrphelin final: public Executor {
	std::mutex _mutex;
	std::list<std::move_only_function<void(void)>> operations;

public:
	ExecuteurOrphelin() {}

	ExecuteurOrphelin(ExecuteurOrphelin const &) = delete;
	ExecuteurOrphelin(ExecuteurOrphelin &&) = delete;
	~ExecuteurOrphelin() = default;

	ExecuteurOrphelin & operator=(ExecuteurOrphelin const &) = delete;
	ExecuteurOrphelin & operator=(ExecuteurOrphelin &&) = delete;

	void execute(std::move_only_function<void(void)> && f) noexcept override;

	bool empty() const noexcept { return operations.empty(); }

	/** \brief Exécute une tâche dans le thread courant. */
	void run() noexcept;

	/** \brief Exécute des tâches jusqu’à ce que la condition soit vraie.
	 *
	 * Cette fonction peut être utilisée pour prêter des ressources jusqu’à ce qu’une tâche injectée précédemment soit exécutée et terminée.
	*/
	void run(bool (*test)() noexcept) noexcept;
}; //// class ExecuteurOrphelin

} // namespace universal::parallel
