#include "executor.hpp"
#include "universal/parallel/api.h"

#include <chrono> // thread_pool::survie
#include <condition_variable> // thread_pool::_taches_cv
#include <list>
#include <mutex> // thread_pool::_taches_mutex
#include <shared_mutex> // thread_pool::_unites_mutex
#include <stop_token> // std::stop_source, std::stop_token
#include <thread> // std::jthread
#include <vector>

namespace universal::parallel {

/** \brief Un ensemble fixe de thread pour exécuter des tâches.
 *
 * Le nombre de threads de travail est fixé à l’initialisation et les threads sont maintenus en vie durant toute l’existence de cet objet.
 *
 * \par Tâches
 * Les nouvelles tâches sont soumise au groupe par la fonction \ref thread_pool::execute.
 * Il n’y a aucun garantie que les tâches soient exécutées dans l’ordre dans lequel elles ont été déposées.
 *
 * \par Extinction
 * Le groupe peut demander à être interrompu, ce qui est fait automatiquement à la destruction.
 * Pendant une interruption douce, le groupe n’accepte pas plus de nouvelles tâches mais termine toutes celles en cours.
 * L’extinction est un processus bloquant tant que tous les threads de travail sont actifs.
 */
class UNI_PARALLEL_API thread_pool: public Executor {
	class thread_pool_unit: public std::jthread {
	public:
		thread_pool * const pool; /**< groupe de travail */

		enum activite { ATTENTE, CALCUL, TERMINE };

		volatile activite etat = ATTENTE;

	public:
		explicit thread_pool_unit(thread_pool *) noexcept;
		/** \brief Crée une nouvelle unité dans le même groupe de travail. */
		thread_pool_unit(thread_pool_unit const &) = delete;
		thread_pool_unit(thread_pool_unit &&) = delete;
		~thread_pool_unit() = default;

		thread_pool_unit & operator=(thread_pool_unit const &) = delete;
		thread_pool_unit & operator=(thread_pool_unit &&) = delete;

	protected:
		/** \brief Boucle infinie de l’unité.
	 *
	 * Demande à la thread_pool une nouvelle tâche à réaliser.
	 * Si la tâche est vide, termine la boucle et ferme le thread.
	 */
		void operator()(std::stop_token) noexcept;
	}; //// thread_pool_unit

private:
	enum activite { PRET, EXTINCTION, TERMINE };

	volatile activite etat_ = PRET;

	// Configuration
	size_t const max_thread;
	std::chrono::milliseconds survie{5000};

	mutable std::shared_mutex _unites_mutex;
	std::vector<std::unique_ptr<thread_pool_unit>> _unites; /**< Unités de travail connues. */

	/** \brief Liste des tâches à exécuter.
	 *
	 * Cette liste doit être bloquante si elle ne contient plus d’éléments.
	 */
	std::list<std::move_only_function<void()>> _taches;
	std::mutex _taches_mutex; // TODO: utiliser une liste sans verrouillage
	std::condition_variable _taches_cv;

	/** \brief Trouve une nouvelle tache pour un travailleur.
	 *
	 * Cette fonction est bloquante pour celui qui l'interroge tant qu'il n'y a
	 * pas de tâche à lui faire exécuter.
	 *
	 * Un travailleur recevra l'ordre de s'éteindre si la thread_pool tente de se fermer,
	 * ou si aucun travail n'est disponible après avoir dépasser la limite d'attente.
	 *
	 * \return std::function vide pour éteindre le thread de travail.
	 */
	std::move_only_function<void()> tache() noexcept;

	void nouvelle_unite();

	static bool unite_inactive(std::unique_ptr<thread_pool::thread_pool_unit> const & unit);

public:
	thread_pool(size_t = 4) noexcept;
	thread_pool(thread_pool const &) = delete;
	thread_pool(thread_pool &&) = delete;
	~thread_pool();

	thread_pool & operator=(thread_pool const &) = delete;
	thread_pool & operator=(thread_pool &&) = delete;

	size_t size() const noexcept;

	/** \brief Enregistre une nouvelle tâche à exécuter dès que possible. */
	void execute(std::move_only_function<void()> &&) override;
}; //// thread_pool

}; // namespace universal::parallel
