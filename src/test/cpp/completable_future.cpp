#include "universal/parallel/completable_future.hpp"
#include "universal/parallel/executors.hpp"
#include "gtest/gtest.h"
#include <algorithm>
#include <iterator>
#include <random>
#include <span>
#include <vector>

using namespace universal::parallel;


class DestroyExecutor : public Executor {
	void execute(std::move_only_function<void(void)> &&) noexcept override {  };
};


TEST(CompletableFuture, complete) {
	auto i = CompletableFuture<int>::completed(138);
	auto pi = CompletableFuture<float>::supply([](){ return 3.1415f; });

	EXPECT_TRUE(i.isDone());
	EXPECT_FALSE(i.isCancelled());
	EXPECT_FALSE(i.complete(18));
	// pi.isDone() unknown
	EXPECT_FALSE(pi.isCancelled());
}

TEST(CompletableFuture, fibonacci) {
	auto u1 = CompletableFuture<unsigned int>::completed(1);
	auto u2 = u1;

	static auto add = [](unsigned int a, unsigned int b) -> unsigned int { return a + b; };
	for (auto i = 2u; i <= 15; ++i) {
		auto u3 = u1.thenCombine(u2, add);
		u1 = std::move(u2);
		u2 = std::move(u3);
	}

	ASSERT_TRUE(u2.isDone());
	EXPECT_EQ(987, u2.get());
}

template<typename T>
std::vector<T> fusion(std::vector<T> const & r1, std::vector<T> const & r2) {
	std::vector<T> resultat;
	std::merge(r1.begin(), r1.end(), r2.begin(), r2.end(), std::back_inserter(resultat));
	return resultat;
}

template<typename T>
CompletableFuture<std::vector<T>> sort (std::vector<T> const & r, Executor * executor) {
	if (r.size() <= 16) {
		return CompletableFuture<std::vector<T>>::supply(
		  [data = std::vector<T>(r)]() mutable {
			  std::sort(data.begin(), data.end());
			  return data;
		  },
		  executor);
	} else {
		auto milieu = r.begin();
		std::advance(milieu, r.size() / 2);
		std::vector<T> v1;
		std::vector<T> v2;
		std::copy(r.begin(), milieu, std::back_inserter(v1));
		std::copy(milieu, r.end(), std::back_inserter(v2));

		return sort(v1, executor).thenCombine(sort(v2, executor), &fusion<T>, executor);
	}
};

TEST(CompletableFuture, sort) {
	ExecuteurOrphelin executor;
	std::default_random_engine rand_engine;
	std::uniform_int_distribution<unsigned int> rand;
	std::vector<unsigned int> data (4096);

	std::generate(data.begin(), data.end(), [&](){ return rand(rand_engine); });

	size_t compte = 0;
	auto tri = sort<unsigned int>(data, &executor);

	while (!executor.empty()) {
		executor.run();
		compte++;
	}
	std::cout << compte << std::endl;

	ASSERT_TRUE(tri.isDone());
	EXPECT_TRUE(std::is_sorted(tri.get().begin(), tri.get().end()));
}


TEST(CompletableFuture, accept) {
	auto c1 = CompletableFuture<float>::completed(3.1415f);
	auto c2 = c1.thenAccept([](float i){});

	EXPECT_TRUE(c2.isDone());
}


TEST(CompletableFuture, accept_both) {
	auto c1 = CompletableFuture<unsigned int>::completed(18);
	auto c2 = CompletableFuture<unsigned int>::completed(84);

	auto c3 = c1.thenAcceptBoth(c2, [](unsigned int v1, unsigned int v2) { std::cout << v1 + v2 << std::endl; });

	EXPECT_TRUE(c3.isDone());
}

/**
 * \brief Test de compilation
 *
 */
TEST(CompletableFuture, void_) {
	auto const c1 = CompletableFuture<void>::completed();
	auto const c2 = CompletableFuture<void>::supply([]() { ; });

	auto c3 = c1.thenCombine(c2, []() { ; });
	auto c4 = c2.thenRun([]() { ; });
}
