#include "universal/parallel/container/concurrent_queue.hpp"
#include "gtest/gtest.h"
#include <algorithm>
#include <memory>
#include <random>
#include <thread>
#include <vector>

using namespace universal::parallel;

TEST(Queue, init) {
	std::unique_ptr<FileBloquante<int>> test;

	ASSERT_NO_THROW(test = std::make_unique<FileBloquante<int>>());
	EXPECT_TRUE(test->empty());
	test->push(215);
	ASSERT_NO_THROW(test.reset());
}


TEST(Queue, sequence) {
	FileBloquante<int> test;

	test.push(78);
	test.push(136);
	test.push(-54);

	EXPECT_EQ(78, test.pop());
	EXPECT_EQ(136, test.pop());
	EXPECT_EQ(-54, test.pop());
	EXPECT_TRUE(test.empty());
}


TEST(Queue, multiple_sequence) {
	FileBloquante<float> test;
	std::vector<float> cibles;
	std::random_device r;
	std::uniform_real_distribution<float> urd (0, 128);
	std::generate_n(std::back_inserter(cibles), 128, [&](){ return urd(r); });

	size_t i = 0;
	size_t j = 0;
	for(; i < 128; ++i) {
		test.push(std::move(cibles[i]));

		if (i % 16 == 15) {
			EXPECT_FALSE(test.empty());
			for (size_t a = 0; a < 8 && j < i; ++a, ++j)
				EXPECT_EQ(cibles[j], test.pop());
			EXPECT_FALSE(test.empty());
		}
	}

	while (j < i) {
		EXPECT_EQ(cibles[j], test.pop());
		++j;
	}

	EXPECT_TRUE(test.empty());
}


TEST(Queue, injection_parallele) {
	FileBloquante<size_t> queue;

	static auto injection = [&queue]() {
		for (size_t i = 0; i < 512; ++i) {
			queue.push(i);
			std::this_thread::yield();
		}
	};

	std::thread injecteur_1 (injection);
	std::thread injecteur_2 (injection);
	std::thread injecteur_3 (injection);

	size_t consomme = 0;
	while (consomme < 3 * 512) {
		queue.pop();
		++consomme;
	}

	injecteur_1.join();
	injecteur_2.join();
	injecteur_3.join();
	EXPECT_TRUE(queue.empty());
	EXPECT_EQ(3 * 512, consomme);
}


TEST(Queue, extraction_parallele) {
	FileBloquante<size_t> queue;

	static auto extraction = [&queue]() {
		for (size_t i = 0; i < 512; ++i) {
			queue.pop();
			std::this_thread::yield();
		}
	};

	std::thread injecteur_1 (extraction);
	std::thread injecteur_2 (extraction);
	std::thread injecteur_3 (extraction);

	size_t injection = 0;
	while (injection < 3 * 512) {
		queue.push(51684);
		++injection;
	}

	injecteur_1.join();
	injecteur_2.join();
	injecteur_3.join();
	EXPECT_TRUE(queue.empty());
	EXPECT_EQ(3 * 512, injection);
}