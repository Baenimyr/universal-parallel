#include "gtest/gtest.h"
#include "universal/move_function.hpp"

TEST(MoveFunction, creation) {
	std::move_only_function<int(int, int)> add ([](int i, int j) -> int { return i + j; });

	EXPECT_EQ(9, add(6, 3));
}