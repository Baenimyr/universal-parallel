#include "universal/parallel/thread_pool.hpp"
#include "gtest/gtest.h"
#include <algorithm>
#include <future>
#include <random>
#include <thread>
#include <vector>

using namespace universal::parallel;

TEST(Thread_Pool, mini) {
	thread_pool p (2);

	static auto const test = [](){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	};

	p.execute(test);
	p.execute(test);
	p.execute(test);
	p.execute(test);
}


TEST(Thread_Pool, bloquage) {
	thread_pool p (1);

	static auto const test = [](){
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	};

	p.execute(test);
	p.execute(test);
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	p.execute(test);
}


TEST(Thread_Pool, travail) {
	constexpr size_t taille = 1 << 12;
	std::vector<unsigned int> data (taille);

	{
		unsigned int i = 0;
		std::generate(data.begin(), data.end(), [&i](){ return ++i; });
	}

	ASSERT_TRUE(std::is_sorted(data.begin(), data.end()));

	auto const melange = [](std::vector<unsigned int>::iterator b, std::vector<unsigned int>::iterator e) {
		std::random_device rand;
		std::mt19937 r(rand());
		std::shuffle(b, e, r);
	};

	auto const rangement = [](std::vector<unsigned int>::iterator b, std::vector<unsigned int>::iterator e) {
		std::sort(b, e);
	};

	{
		constexpr size_t lot = 1 << 7;
		thread_pool pool (4);
		std::vector<std::future<void>> melanges;
		for (size_t j = 0; j < taille / lot; ++j) {
			std::promise<void> P;
			std::future<void> f = P.get_future();
			auto action = std::bind_front(melange, data.begin() + j * lot, data.begin() + (j + 1) * lot);
			pool.execute([promise = std::move(P), f = std::move(action)]() mutable {
				f();
				promise.set_value();
			});
			melanges.emplace_back(std::move(f));
		}

		for (auto const& fut : melanges)
			fut.wait();

		EXPECT_FALSE(std::is_sorted(data.begin(), data.end()));

		for (size_t j = 0; j < taille / lot / 2; ++j) {
			pool.execute(std::bind_front(rangement, data.begin() + j * lot * 2, data.begin() + (j + 1) * lot * 2));
		}
	}

	EXPECT_TRUE(std::is_sorted(data.begin(), data.end()));
}